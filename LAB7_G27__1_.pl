% Define the number of days in each month
% It assigns the number of days to each month represented by its corresponding number.
days_in_month(1, 31).
days_in_month(2, 28).
days_in_month(3, 31).
days_in_month(4, 30).
days_in_month(5, 31).
days_in_month(6, 30).
days_in_month(7, 31).
days_in_month(8, 31).
days_in_month(9, 30).
days_in_month(10, 31).
days_in_month(11, 30).
days_in_month(12, 31).

% Define the predicate to calculate the number of days between two dates
%The interval/3 predicate is the main predicate that takes two dates as input, represented as strings Start and End, 
%and calculates the number of days between them. 
interval(Start, End, Days) :-
    %It first calls the parse_date/3 predicate to convert the dates into their day and month components. 
    parse_date(Start, Day1, Month1),
    parse_date(End, Day2, Month2),
    %Then, it calls the calculate_days/5 predicate to perform the actual calculation.
    calculate_days(Day1, Month1, Day2, Month2, Days).

% Define the predicate to parse the date into day and month components
%The parse_date/3 predicate takes a date represented as a string (Date) and converts it into its day and month components (Day and Month). 
parse_date(Date, Day, Month) :-
    atom_chars(Date, [D1, D2, M1, M2]),
    %It uses atom_chars/2 predicate to split the string into individual characters and 
    atom_chars(DayAtom, [D1,D2]),
    atom_chars(MonthAtom, [M1,M2]),
    %then converts them to atoms using atom_number/2.
    atom_number(DayAtom, Day),
    atom_number(MonthAtom, Month).

% Define the predicate to calculate the number of days between two dates
calculate_days(Day1, Month1, Day2, Month2, Days) :-
    %This predicate handles the case when both dates fall within the same month. 
    Month1 =< Month2,
    %It checks if Month1 is less than or equal to Month2 and if they are equal. 
    Month1 = Month2,
    %If they are, it calculates the number of days by subtracting Day1 from Day2.
    Days is Day2 - Day1.

% Define the predicate to calculate the number of days between two dates
calculate_days(Day1, Month1, Day2, Month2, Days) :-
    %This predicate handles the case when the start month is earlier than the end month. 
    Month1 < Month2,
    %It calculates the number of days between the start and end dates by considering the full months between them.
    days_in_month(Month1, DaysInMonth1),
    %First, it calculates Days1 as the number of days remaining in the starting month after Day1. 
    Days1 is DaysInMonth1 - Day1 +1,
    %It then calls the calculate_days_in_between/3 predicate to determine the number of days in the full months between Month1 and Month2.
    calculate_days_in_between(Month1, Month2, DaysBetween),
    %Next, it calculates Days2 as the number of days that have passed in the end month before Day2. 
    Days2 is Day2 - 1,
    %Finally, it calculates the total number of days (Days) by adding Days1, DaysBetween, Days2, and subtracting 30. 
	%The subtraction of 30 is done to account for an extra 30 days that were mistakenly added.
    Days is (Days1 + DaysBetween + Days2 -30).

%This predicate handles the case when the start month is later than the end month. 
%It swaps the start and end dates and calls the calculate_days/5 predicate again to calculate the number of days between them.
calculate_days(Day1, Month1, Day2, Month2, Days) :-
    Month1 > Month2,
    calculate_days(Day2, Month2, Day1, Month1, Days).

% Define the predicate to calculate the number of days between same months
% calculate_days_in_between/3 predicate is reached when the start month and end month are the same. 
%It simply assigns 0 to the Days variable.
calculate_days_in_between(Month, Month, 0).

%This recursive case of the calculate_days_in_between/3 predicate is reached when the start month is earlier than the end month. 
%It calculates the number of days between consecutive months by adding the number of days in Month1 to the remaining days between the next month (NextMonth) and Month2. 
calculate_days_in_between(Month1, Month2, Days) :-
    Month1 < Month2,
    NextMonth is Month1 + 1,
    %The number of days in Month1 is obtained using the days_in_month/2 predicate. 
    days_in_month(Month1, DaysInMonth1),
    %The remaining days are calculated recursively by calling calculate_days_in_between/3 with the incremented NextMonth. 
    calculate_days_in_between(NextMonth, Month2, RemainingDays),
    %The total number of days is assigned to the Days variable after subtracting 1 to account for the overlapping day.
    Days is DaysInMonth1 + RemainingDays - 1.

% EXAMPLES
% how many days between 22nd of May and 5th of June
% ?- interval("2205", "0506",Days).
% Days = 14

% how many days between 1st of Feb and 11th of Feb
% ?- interval("0102", "1102", Days).
% Days = 10

% how many days between 25nd of May and 26th of May
% ?- interval("2507", "2607", Days).
% Days = 1

% how many days between 25nd of July and 25nd of August
% ?- interval("2507", "2508", Days).
% Days = 31

